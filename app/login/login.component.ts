import { Router } from "@angular/router";
import { Component } from "@angular/core";
import { User } from "../shared/user/user.model";
import { UserService } from "../shared/user/user.service";
@Component({
    selector: "gr-login",
    providers: [UserService],
    styleUrls: ["login/login.component.css"],
    templateUrl: "login/login.component.html"
})
/* export class LoginComponent {
  email = "nativescriptrocks@progress.com";//variable declaration
  submit() { //when login button is clicked 
    alert("You’re using: " + this.email);
  }
}*/
export class LoginComponent {
    user: User;
    isLoggingIn = true;

    constructor(private router: Router, private userService: UserService) {
        this.user = new User();
     }



    toggleDisplay() {
        this.router.navigateByUrl('/list');
        
    }

    submit() {
        if (this.isLoggingIn) {
            this.login();
        } else {
            this.signUp();
        }
    }

        login() {
            this.userService.register(this.user)
                .subscribe(
                    () => this.router.navigate(["/list"]),
                    (error) => alert("Unfortunately we could not find your account.")
                );
        }

    signUp() {
        this.userService.register(this.user)
            .subscribe(
                () => {
                    alert("Your account was successfully created.");
                    this.toggleDisplay();
                },
                () => alert("Unfortunately we were unable to create your account.")
            );
    }
}